<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="dipsraj">
    <meta name="theme-color" content="#343a40">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-title" content="dipsraj">
    <meta name="apple-mobile-web-app-status-bar-style" content="#343a40">
    <!-- Tile icon for Win8 -->
    <meta name="msapplication-TileColor" content="#343a40">
    <meta name="msapplication-navbutton-color" content="#cacaca">

    <title>DIPSRAJ</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="assets/css/agency.min.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/loader.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- Bootstrap core JavaScript -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <script src="assets/vendor/sweetalert2.all.js"></script>
    <script src="assets/js/scripts.js"></script>

</head>

<body id="page-top">

<div class="login-loader" id="loader-wrapper">
    <div class="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">Dipsraj</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#page-top">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Welcome To Dipsraj's Studio!</div>
            <div class="intro-heading text-uppercase">Full site coming soon</div>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#about">Tell Me More</a>
        </div>
    </div>
</header>

<?php

if (empty($_SESSION)) {
    session_start();
}

if (isset($_POST["email"]) && $_POST["subject"] && $_POST["msg"]){

    $name = $_POST["name"];
    $email = $_POST["email"];
    $sub = $_POST["subject"];
    $msg = $_POST["msg"];

    require_once 'mail.php';

    $result = send_my_mail($name, $email, $sub, $msg);

    if($result == 2){
        echo "<script>swal({ type: 'success', title: 'Email Received', text: 'We will contact you soon.', allowOutsideClick: () => { return false; } }).then((result) => {  if (result.value) { window.location.href = '/';} })</script>";
    }
    else {
        echo "<script>swal({ type: 'error', title: 'Some error occurred', text: 'Please try again later.', allowOutsideClick: () => { return false; } }).then((result) => {  if (result.value) { window.location.href = '/';} })</script>";
    }

}

else {

?>

<!-- About -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">About</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12" style="display: flex; justify-content: center;">
                <div class="card modal-sm" id="card">
                    <img class="card-img-top" src="assets/img/avatar.png" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Dipanjan Kundu</h4>
                        <p class="card-text">You can call me dipsraj.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Get in touch</h2>
                <h3 class="section-subheading text-muted">Submit the form and we will contact back soon.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form action="/" method="post" id="contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" name="name" type="text" placeholder="Your Name *" required
                                       data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" name="email" type="email" placeholder="Your Email *" required
                                       data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" id="subject" name="subject" placeholder="Subject *" required
                                       data-validation-required-message="Please enter the subject for your message.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" id="msg" name="msg" placeholder="Your Message *" required
                                          data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button id="send-mail" class="btn btn-primary btn-xl text-uppercase" type="submit">
                                Send Message
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

    <?php
}
?>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <span class="copyright">Copyright &copy; <a href="/" style="color: #111;">dipsraj.tk</a> 2018</span>
            </div>
            <div class="col-md-6">
                <ul class="list-inline social-buttons">
                    <li class="list-inline-item">
                        <a href="https://twitter.com/dipsraj_kundu" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/dipanjan1994" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.linkedin.com/in/dipanjan-kundu-9134b110b" target="_blank">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Contact form JavaScript -->
<script src="assets/js/jqBootstrapValidation.js"></script>
<script src="assets/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="assets/js/agency.min.js"></script>

</body>

</html>
