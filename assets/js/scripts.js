$(function () {
    $("#send-mail").click(function (e) {
        e.preventDefault();

        if ($("#email").val() && $("#subject").val() && $("#msg").val()) {
            $("#contact-form").submit();
            $("#loader-wrapper").addClass('is-active');
            $('body').addClass("no-scroll");
        }
        else {
            swal({
                type: 'error',
                title: 'Empty values not allowed',
                text: 'Please fill out the form first.',
                allowOutsideClick: () => {
                    return false;
                }
            })
        }
    });
    $(".alert-coming-soon").click(function (e) {
        e.preventDefault();
        swal({
            type: 'info',
            title: 'Coming Soon',
        })
    });
});